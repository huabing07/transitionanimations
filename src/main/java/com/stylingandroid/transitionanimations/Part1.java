package com.stylingandroid.transitionanimations;

import android.app.Fragment;
import android.os.Bundle;
import android.transition.ChangeBounds;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;

public class Part1 extends Fragment implements View.OnClickListener {
	private ViewGroup mLayout1;
	private ViewGroup mLayout2;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_part1, container, false);
		mLayout1 = (ViewGroup) view.findViewById(R.id.layout_1);
		mLayout1.findViewById(R.id.item_1a).setOnClickListener(this);
		mLayout1.findViewById(R.id.item_1b).setOnClickListener(this);
		mLayout2 = (ViewGroup) view.findViewById(R.id.layout_2);
		mLayout2.findViewById(R.id.item_2a).setOnClickListener(this);
		mLayout2.findViewById(R.id.item_2b).setOnClickListener(this);
		mLayout2.findViewById(R.id.item_2c).setOnClickListener(this);
		return view;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.item_1a || v.getId() == R.id.item_1b) {
			int selected = mLayout1.indexOfChild(v);
			TransitionManager.beginDelayedTransition(mLayout1, new ChangeBounds());
			mLayout1.removeView(v);
			mLayout1.addView(v, selected == 0 ? mLayout1.getChildCount() : 0);
		} else {
			int selected = mLayout2.indexOfChild(v);
			if(selected > 0) {
				View current = mLayout2.getChildAt(0);
				int currentId = current.getId();
				int selectedId = v.getId();
				TransitionManager.beginDelayedTransition(mLayout2, new ChangeBounds());
				mLayout2.removeView(v);
				mLayout2.addView(v, 0);
				GridLayout.LayoutParams params = (GridLayout.LayoutParams)current.getLayoutParams();
				current.setLayoutParams(v.getLayoutParams());
				v.setLayoutParams(params);
				if(currentId == R.id.item_2c || (currentId == R.id.item_2b && selectedId == R.id.item_2c)) {
					mLayout2.removeView(current);
					mLayout2.addView(current);
				}
			}
		}
	}
}
